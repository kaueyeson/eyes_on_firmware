import bluetooth
import subprocess
from Wifi.main import connect
from Util.Messages.Error import NotFound
import logging
import logging.handlers

logger = logging.getLogger("log")
logger.setLevel(logging.INFO)

streamHandler = logging.StreamHandler()
logger.addHandler(streamHandler)

def makeSockForBt():
  serverSock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
  serverSock.bind(("", 1))

  logger.info("listening...")

  serverSock.listen(1)
  port = serverSock.getsockname()[1]
  uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"
  subprocess.call('sudo hciconfig hci0 piscan', shell = True)

  bluetooth.advertise_service(serverSock, "eyes-on-firmware-bluetooth",
                         service_id = uuid,
                         service_classes = [uuid, bluetooth.SERIAL_PORT_CLASS],
                         profiles = [bluetooth.SERIAL_PORT_PROFILE])

  logger.info("Waitng for connection on RFCOMM channel %d" % port)

  clientSock, clientInfo = serverSock.accept()
  logger.info("Accepted connection from %s", clientInfo)
  return serverSock, clientSock

def tryToConnectWifi(serverSock, clientSock):
  try:
    while(True):
      data = clientSock.recv(1024)
      ssid, password = data.split()
      if not connect(ssid, password):
        logger.info("wifi connection fail")
        clientSock.send("fail")
      else:
        logger.info("wifi connection success")
        clientSock.send("success")
        break
    clientSock.close()
    serverSock.close()
    logger.info("bluetooth socket close")
  except NotFound as e: 
    logger.error(e)
    clientSock.send("not found speicific ssid. input other ssid and password.")
    tryToConnectWifi(serverSock, clientSock)
