#!/bin/sh

Log=/home/pi/Desktop/Eyes_on/eyes_on_firmware/Log/test.out
DATE=`date +%Y%m%d-%H%M%S`

echo "#################################################" >> $Log
echo "  Stop Eyes-on"

Cnt=`ps -ex|grep "python3 /home/pi/Desktop/Eyes_on/eyes_on_firmware/MQTT_Publisher.py"|grep -v grep|wc -l`
PROCESS=`ps -ex|grep "python3 /home/pi/Desktop/Eyes_on/eyes_on_firmware/MQTT_Publisher.py"|grep -v grep|awk '{print $1}'`

if [ $Cnt -ne 0 ]
then
	kill -9 $PROCESS
	echo "$DATE : Eyes-on has been stopped." >> $Log
else
	echo "Eyes-on is not running" >> $Log
fi

echo "#################################################" >> $Log
