import paho.mqtt.client as mqtt
import time
import json
from collections import OrderedDict

def on_connect(client, userdata, flags, rc):
    m = "Connected flags" + str(flags) + "result code " + str(rc) + "client1_id " + str(client)
    print(m)

def on_message(client1, userdata, message):
    print("message received ", str(message.payload.decode("utf-8")))
##
sensor_data = OrderedDict()
data = OrderedDict()

sensor_data["device_type"] = "eyes-on-temp-humid"

data["temp"] = 25.0
data["humid"] = 35.0

sensor_data["data"] = data

json_sensor_data = json.dumps(sensor_data, indent="\t")
print(json_sensor_data)
#print(json.dumps(sensor_data, indent="\t"))

##
broker_address="ec2-34-209-82-50.us-west-2.compute.amazonaws.com"
client1 = mqtt.Client("P1")
client1.on_connect = on_connect
client1.on_message = on_message
time.sleep(1)
client1.connect(broker_address)
client1.loop_start()
#client1.subscribe("house/bulbs/bulb1")
client1.publish("house/bulbs/bulb1", json_sensor_data)
#client1.subscribe("house/bulbs/bulb1")
time.sleep(5)
client1.disconnect()
client1.loop_stop()

                  
