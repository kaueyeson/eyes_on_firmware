
class NotFound(Exception):
  def __init__(self, errMsg):
    self.message = errMsg
  def __str__(self):
    return self.message

class Unprocessable(Exception):
  def __init__(self, errMsg):
    self.message = errMsg
  def __str__(self):
    return self.message

class BadRequest(Exception):
  def __init__(self, errMsg):
    self.message = errMsg
  def __str__(self):
    return self.message