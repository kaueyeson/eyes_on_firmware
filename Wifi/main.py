import wifi
from Util.Messages.Error import *

def search():
    wifilist = []
    cells = wifi.Cell.all('wlan0')

    for cell in cells:
        wifilist.append(cell)

    return wifilist

def findFromSearchList(ssid, searchList):
    for cell in searchList:
        if cell.ssid == ssid:
            return cell
    return False

def findFromSavedList(ssid):
    cell = wifi.Scheme.find('wlan0', ssid)

    if cell:
        return cell
    return False

def connect(ssid, password = None):
    searchList = search()
    cell = findFromSearchList(ssid, searchList)

    if cell == False:
        raise NotFound("not found specific wifi ssid nearby this place")
    
    savedCell = findFromSavedList(cell.ssid)
    
    if cell and savedCell != False:
        savedCell.activate()
    elif cell and savedCell == False:
        if cell.encrypted:
            if password:
                scheme = add(cell, password)

                try:
                    scheme.activate()
                except wifi.exceptions.ConnectionError:
                    delete(ssid)
                    return False
                return cell
        
    else: False

def add(cell, password = None):
    if not cell:
        return False

    scheme = wifi.Scheme.for_cell('wlan0', cell.ssid, cell, password)
    scheme.save()
    return scheme

def delete(ssid):
    if not ssid:
        return False

    cell = findFromSavedList(ssid)

    if cell:
        cell.delete()
        return True
    return False

