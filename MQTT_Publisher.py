import serial
import sys
sys.path.append("/usr/local/lib/python3.5/dist-packages")
import paho.mqtt.client as mqtt
import time
import json
from collections import OrderedDict

def on_connect(client, userdata, flags, rc):
	m = "Connected flags" + str(flags) +"result code " + str(rc) + "client_id " + str(client)
	print(m)

def on_message(client1, userdata, message):
	print("message received ", str(message.payload.decode("utf-8")))

dev = OrderedDict()
sensor_value = OrderedDict()
detection = OrderedDict()

pool = list()
pSize = 60

port = serial.Serial("/dev/ttyS0", 9600)

while True:
	try:
		print("Wating data...")

		res = port.readline()
		response = res[:-2].decode()

		soil = response.split(',')[0]
		water = response.split(',')[1]
		humid = response.split(',')[2]
		temp = response.split(',')[3]
		device_type = response.split(',')[4]
		device_id = response.split(',')[5]

		nPool = len(pool)
		water_Detect = False
		refresh_Detect = False

		if nPool!=0:
			if nPool<pSize:
				pool.append(int(water))
			elif int(water)>100:
				pool = list()
				pool.append(int(water))
				water_Detect = True
			else:
				pool = list()
		elif int(water)>100:
			pool.append(int(water))
			water_Detect = True

		dev["device_type"] = str(device_type)
		dev["device_id"] = int(device_id)

		sensor_value["soil"] = float(soil)
		sensor_value["temp"] = float(temp)
		sensor_value["humid"] = float(humid)

		detection["water_detection"] = bool(water_Detect)
		detection["refresh_detection"] = bool(refresh_Detect)

		dev["sensor_value"] = sensor_value
		dev["detection"] = detection

		json_dev_data = json.dumps(dev, indent="\t")

		broker_address="ec2-52-10-70-62.us-west-2.compute.amazonaws.com"
		client1 = mqtt.Client("Dev_1")
		client1.on_connect = on_connect
		client1.on_message = on_message
		time.sleep(1)
		client1.connect(broker_address)

		client1.publish("devices/%s/%d/events/raw-data" % (str(device_type), int(device_id)), json_dev_data)
		time.sleep(5)

		print(json_dev_data)

	except KeyboardInterrupt:
		break

port.close()
