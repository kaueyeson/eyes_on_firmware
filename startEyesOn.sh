#!/bin/sh

Log=/home/pi/Desktop/Eyes_on/eyes_on_firmware/Log/test.out
DATE=`date +%Y%m%d-%H%M%S`

mv -f "$Log" "${Log}_`date +%Y-%m-%d-%H%M`"

echo "#################################################" >> $Log
echo "   Start Eyes-on"

Cnt=`ps -ex|grep "python3 /home/pi/Desktop/Eyes_on/eyes_on_firmware/MQTT_Publisher.py"|grep -v grep|wc -l`
PROCESS=`ps -ex|grep "python3 /home/pi/Desktop/Eyes_on/eyes_on_firmware/MQTT_Publisher.py"|grep -v grep|awk '{print $1}'`

if [ $Cnt -ne 0 ]
then
	echo "$DATE : Eyes-on(PID : $PROCESS) is running already." >> $Log
else
	python3 /home/pi/Desktop/Eyes_on/eyes_on_firmware/MQTT_Publisher.py >> $Log &
	echo "$DATE : Eyes-on has been started." >> $Log
fi

echo "#################################################" >> $Log
