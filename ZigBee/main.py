import time
import serial
import json
from collections import OrderedDict

dev = OrderedDict()
sensor_value = OrderedDict()
detection = OrderedDict()

pool = list()
pSize = 5

port = serial.Serial("/dev/ttyS0", 9600)

while True:
	try:
		print("Wating data...")

		res = port.readline()
		response = res[:-2].decode()

		soil = response.split(',')[0]
		water = response.split(',')[1]
		humid = response.split(',')[2]
		temp = response.split(',')[3]
		device_type = response.split(',')[4]
		device_id = response.split(',')[5]

		nPool = len(pool)
		water_Detect = False
		refresh_Detect = False

		if nPool!=0:
			if nPool<pSize:
				pool.append(int(water))
			elif int(water)>100:
				pool = list()
				pool.append(int(water))
				water_Detect = True
			else:
				pool = list()
		elif int(water)>100:
			pool.append(int(water))
			water_Detect = True

		print(pool)
		print(int(water))

		dev["device_type"] = str(device_type)
		dev["device_id"] = int(device_id)

		sensor_value["soil"] = float(soil)
		sensor_value["temp"] = float(temp)
		sensor_value["humid"] = float(humid)

		detection["water_detection"] = bool(water_Detect)
		detection["refresh_detection"] = bool(refresh_Detect)

		dev["sensor_value"] = sensor_value
		dev["detection"] = detection

		json_dev_data = json.dumps(dev, indent="\t")
		print(json_dev_data)


	except KeyboardInterrupt:
		break

port.close()
