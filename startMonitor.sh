#!/bin/bash

DATE=`date +%Y%m%d-%H%M%S`
Log=/home/pi/Desktop/Eyes_on/eyes_on_firmware/Log/monitor.out

NORMAL_SLEEP=60
PROB_SLEEP=5

mv -f "$Log" "${Log}_`date +%Y-%m-%d-%H%M`"

echo "#################################################" >> $Log
echo "$DATE Eyes-on Monitor Start!" >> $Log
echo "#################################################" >> $Log

while [ 1 ]
do
	DATE=`date +%Y%m%d-%H%M%S`
	Cnt=`ps -ex|grep "python3 /home/pi/Desktop/Eyes_on/eyes_on_firmware/MQTT_Publisher.py"|grep -v grep|wc -l`

	if [ $Cnt \< 1 ]
	then
		PROCESS=`ps -ex|grep "python3 /home/pi/Desktop/Eyes_on/eyes_on_firmware/MQTT_Publisher.py"|grep -v grep|awk '{print $1}'`
		if [ "$PROCESS" != "" ]
		then
			echo "$DATE : Eyes-on has been stopped." >> $Log
			. /home/pi/Desktop/Eyes_on/eyes_on_firmware/stopEyesOn.sh &
			wait
		fi

		echo "$DATE : Eyes-on has been re-started." >> $Log
		. /home/pi/Desktop/Eyes_on/eyes_on_firmware/startEyesOn.sh &

		echo "$DATE : Eyes-on restarting has been completed." >> $Log

		sleep $PROB_SLEEP

		continue
	else
		echo "$DATE : Eyes-on in running well." >> $Log
	fi

	sleep $NORMAL_SLEEP
done
